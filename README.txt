-- SUMMARY --

This module provides insert in text "view" with help tag [viewsembfilter]view_name[\viewsembfilter].
For example, you can use this module to display "view" grid in text.
Demo - http://

-- REQUIREMENTS --

views

-- INSTALLATION --

* Tick module on page modules

-- CONFIGURATION --

* Tick settings "Do not show hover links over views" on page admin/build/views/tools for better usability

-- USE --

1. Create "view" for task (example grid).
2. Create your own filter settings which set the processing views embed filter.
3. Insert the text [viewsembfilter]view_name[\viewsembfilter] and check the filter you created.

-- CONTACT --

Developer http://drupal.org/user/203339
This project has been sponsored by:
* JASMiND
  Is an independent professional consulting group with extensive background in telecommunications, IT, software development and implementation. Visit http://jmproduct.ru/en for more information.
* AEROFLOT
  Largest air carrier in Russia. Visit http://aeroflot.ru for more information.